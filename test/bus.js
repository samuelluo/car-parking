var expect = require("chai").expect;
var Bus = require("../app/bus");

describe("bus parking caculate", function() {
  describe("testing caculating", function() {
    it("test x,y,f", function() {
      const bus = new Bus(1,2,'EAST')
      const params = [ 'PLACE 1,2,EAST', 'MOVE', 'MOVE', 'LEFT', 'MOVE', 'REPORT' ]
      const res = bus.calculate(params)
      expect(res).to.equal("x = 3  y = 3 , NORTH");
    });
  });
});