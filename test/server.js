var expect = require("chai").expect;
var request = require("request");
describe("Bus parking API", function () {

  describe("Bus parking get output", function () {
    var url = "http://localhost:8080/getOutput?param1=PLACE 1,2,SOUTH&param2=MOVE&param3=LEFT&param4=RIGHT&param5=REPORT";
    it("returns status 200", function (done) {
      request(url, function (error, response, body) {
        expect(response.statusCode).to.equal(200);
        done();
      });
    });
  });

  describe("Bus parking get index", function () {
  var url = "http://localhost:8080";
    it("returns status 200", function (done) {
      request(url, function (error, response, body) {
        expect(response.statusCode).to.equal(200);
        done();
      });
    });
  });

});