const Faces = ['EAST', 'SOUTH', 'WEST', 'NORTH']

module.exports = class Bus {

  constructor(x, y, f) {
    this.x = x
    this.y = y
    this.f = f
  }

  calculate(params) {
    let report = ''
    for (const param of params) {
      switch (param) {
        case 'MOVE': {
          if (this.f === 'NORTH') {
            if (this.y >= 0 && this.y < 5) {
              this.y++
            }
          } else if (this.f === 'SOUTH') {
            if (this.y > 0 && this.y <= 5) {
              this.y--
            }
          } else if (this.f === 'WEST') {
            if (this.x > 0 && this.x <= 5) {
              this.x--
            }
          } else if (this.f === 'EAST') {
            if (this.x >= 0 && this.x < 5) {
              this.x++
            }
          }
          break;
        }
        case 'LEFT': {
          const index = Faces.indexOf(this.f)
          if (index == 0) {
            this.f = Faces[Faces.length - 1]
          } else {
            this.f = Faces[index - 1]
          }
          break;
        }
        case 'RIGHT': {
          const index = Faces.indexOf(this.f)
          if (index == Faces.length - 1) {
            this.f = Faces[0]
          } else {
            this.f = Faces[index + 1]
          }
          break;
        }
        case 'REPORT': {
          report = report.concat(`x = ${this.x}  y = ${this.y} , ${this.f}`)
          break;
        }
        default: {
          const position = [...param.split(/[ ,]+/)]
          if (position[0] === 'PLACE') {
            this.x = parseInt(position[1])
            this.y = parseInt(position[2])
            this.f = position[3]
          }
          break;
        }
      }
    }
    return report
  }
}
