var express = require("express");
var app = express();
var Bus = require("./bus");

app.get('/', function (req, res) {
  res.sendFile('index.html', {root: __dirname + "/"})
})

app.get("/getOutput", function (req, res) {
  let params = []
  for(const p in req.query){
    params.push(req.query[p])
  }
  //find the first PLACE command, and remove the invalid commands before
  for(const p in req.query){
    const position = [...req.query[p].split(/[ ,]+/)]
    if(position[0] != 'PLACE'){
      delete params[params.indexOf(req.query[p])]
    }else{
      break
    }
  }
  let result
  if(params.length > 0){
    const position = [...params[0].split(/[ ,]+/)]
    const x =  parseInt(position[1])
    const y =  parseInt(position[2])
    const f = position[3]
    if(x >= 0 && x <= 5 && y >= 0 && y <= 5){
      let bus = new Bus(x,y, f)
      console.log("**** param ***",params)
      result = bus.calculate(params)
    }else{
      result = 'Invalid position input'
    }
  }else{
    result = 'Invalid input without PLACE'
  }
  res.send(result)
})

app.listen(8080)